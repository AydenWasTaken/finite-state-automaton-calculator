﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Finite_State_Automata_Calculator
{
    public partial class AutomatonCalculator : Form
    {
        Automaton automaton = null;

        public AutomatonCalculator()
        {
            InitializeComponent();

            // populate preset list from json
            if (File.Exists("./savedAutomatons.bin"))
            {
                using (FileStream stream = new FileStream("./savedAutomatons.bin", FileMode.Open))
                {
                    if (stream.Length > 0)
                    {
                        List<Automaton> savedAutomatons = (List<Automaton>)new BinaryFormatter().Deserialize(stream);
                        savedAutomatons.ForEach(automaton => listBoxSavedAutomatons.Items.Add(automaton));
                    }
                }
            }
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            Calculate();
        }

        private void buttonOpenGraphPicture_Click(object sender, EventArgs e)
        {
            OpenGraphPicture();
        }

        private void buttonSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog ofd = new OpenFileDialog())
                {
                    ofd.InitialDirectory = Directory.GetCurrentDirectory();
                    ofd.Filter = "txt files (*.txt)|*.txt";
                    ofd.FilterIndex = 1;
                    ofd.RestoreDirectory = true;

                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        using (StreamReader reader = new StreamReader(ofd.OpenFile()))
                        {
                            richTextBoxInput.Text = reader.ReadToEnd();
                        }

                        Calculate();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred:\n{ex.Message}");
            }
        }

        private void AutomatonCalculator_FormClosing(object sender, FormClosingEventArgs e)
        {
            // delete temporary files (graph text and png, etc.)
            if (File.Exists("./graph.png"))
                File.Delete("./graph.png");
            if (File.Exists("./graph.dot"))
                File.Delete("./graph.dot");

            using (FileStream stream = new FileStream("./savedAutomatons.bin", FileMode.Create))
            {
                List<Automaton> automatonsToSave = new List<Automaton>();

                foreach (Automaton a in listBoxSavedAutomatons.Items)
                    automatonsToSave.Add(a);

                new BinaryFormatter().Serialize(stream, automatonsToSave);
            }
        }

        private void buttonIsWordPossible_Click(object sender, EventArgs e)
        {
            //if every letter from the input is in the alphabet, do it. Warn the user otherwise
            if (!(textBoxIsWordPossible.Text.ToCharArray().Except(automaton.Alphabet).Count() > 0))
            {
                listboxCheckedPossibleWords.Items.Insert(0, $"{textBoxIsWordPossible.Text} - {automaton.IsWordPossible(textBoxIsWordPossible.Text)}");
            }
            else
            {
                MessageBox.Show("The input contains letters that are not in the alphabet.");
            }
        }

        private void buttonParseRegex_Click(object sender, EventArgs e)
        {
            ParseRegex();
        }

        private void buttonDeleteSelectedAutomaton_Click(object sender, EventArgs e)
        {
            if (listBoxSavedAutomatons.SelectedItem != null)
            {
                listBoxSavedAutomatons.Items.Remove(listBoxSavedAutomatons.SelectedItem);
            }
            else
            {
                MessageBox.Show("No automaton selected.");
            }
        }

        private void buttonSaveAutomaton_Click(object sender, EventArgs e)
        {
            if (automaton != null && !string.IsNullOrWhiteSpace(textBoxAutomatonName.Text))
            {
                foreach (Automaton a in listBoxSavedAutomatons.Items)
                {
                    if (a.Name == textBoxAutomatonName.Text)
                    {
                        MessageBox.Show("Name is already used.");
                        return;
                    }
                }
                automaton.Name = textBoxAutomatonName.Text;
                listBoxSavedAutomatons.Items.Add(automaton);
            }
            else
            {
                MessageBox.Show("No automaton to save or no name to save it by.");
            }
        }

        private void buttonLoadSavedAutomaton_Click(object sender, EventArgs e)
        {
            if (listBoxSavedAutomatons.SelectedItem != null)
            {
                automaton = (Automaton)listBoxSavedAutomatons.SelectedItem;

                UpdateResultsOnForm(automaton);

                GraphHandler graphHandler = new GraphHandler();
                string graphPath = graphHandler.GenerateGraph(automaton);
                pictureBoxGraph.ImageLocation = graphPath;
            }
        }

        private void UpdateResultsOnForm(Automaton automaton)
        {
            // alphabet
            listboxAlphabet.Items.Clear();
            automaton.Alphabet.ForEach(letter => listboxAlphabet.Items.Add(letter));

            // state names
            listboxStateNames.Items.Clear();
            automaton.States.ForEach(state => listboxStateNames.Items.Add(state.Name));

            // possible words
            listboxPossibleWords.Items.Clear();
            automaton.PossibleWords.ForEach(word => listboxPossibleWords.Items.Add(word));

            // input words
            listboxExpectedWords.Items.Clear();
            automaton.InputWords.ToList().ForEach(entry => listboxExpectedWords.Items.Add($"{entry.Key} - {entry.Value}"));

            listboxCorrectExpectedWords.Items.Clear();
            automaton.InputWords.ToList().ForEach(entry =>
            {
                if (automaton.IsWordPossible(entry.Key) == entry.Value)
                {
                    listboxCorrectExpectedWords.Items.Add($"{entry.Key} - {entry.Value}");
                }
            });

            //user tested words
            listboxCheckedPossibleWords.Items.Clear();

            // dfa
            checkBoxDFA.Checked = automaton.IsDFA;
            checkBoxDFAFromInput.Checked = automaton.IsDFAFromFile;
            
            // finite
            checkBoxFinite.Checked = automaton.IsFinite;
            checkBoxFiniteFromFile.Checked = automaton.IsFiniteFromFile;

            // enable controls
            buttonIsWordPossible.Enabled = true;
            buttonOpenGraphPicture.Enabled = true;
            buttonSaveAutomaton.Enabled = true;
            textBoxAutomatonName.Enabled = true;

            if (automaton.IsPDA)
            {
                checkBoxIsPDA.Checked = true;
                listBoxStackLetters.Items.Clear();
                automaton.PossibleLettersStack.ForEach(letter => listBoxStackLetters.Items.Add(letter));
            }
        }

        private void Calculate()
        {
            try
            {
                automaton = new Automaton(richTextBoxInput.Text);

                UpdateResultsOnForm(automaton);

                GraphHandler graphHandler = new GraphHandler();
                string graphPath = graphHandler.GenerateGraph(automaton);
                pictureBoxGraph.ImageLocation = graphPath;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred with the following message:\n{ex.Message}");
            }
        }

        private void ParseRegex()
        {
            try
            {
                RegexHandler regexHandler = new RegexHandler();
                automaton = regexHandler.ParseRegex(richTextBoxInput.Text);

                GraphHandler graphHandler = new GraphHandler();
                string graphPath = graphHandler.GenerateGraph(automaton);
                pictureBoxGraph.ImageLocation = graphPath;

                UpdateResultsOnForm(automaton);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred:\n{ex.Message}");
            }
        }

        private void OpenGraphPicture()
        {
            try
            {
                System.Diagnostics.Process.Start(@".\graph.png");
            }
            catch (Win32Exception)
            {
                MessageBox.Show("No graph found.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred:\n{ex.Message}");
            }
        }
    }
}
