﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Finite_State_Automata_Calculator
{
    class RegexHandler
    {
        private List<State> states;
        private List<char> alphabet;
        private readonly char[] possibleOperators = new char[] { '*', '|', '.' };

        public Automaton ParseRegex(string regex)
        {
            regex = regex.Replace(" ", string.Empty);
            alphabet = new List<char>();
            states = new List<State>();

            alphabet = GetAlphabet(regex);

            State root = new State("start");
            root.SetAsStart();
            states.Add(root);

            State final = new State("final");
            final.SetAsFinal();

            AssignNodes(regex.ToList(), root, final);

            states.Add(final);

            return new Automaton(alphabet, states);
        }

        /// <summary>
        /// Uses the formula given to generate nodes, set their left and right child nodes and adds that node to the list.
        /// </summary>
        private char AssignNodes(List<char> regex, State root, State final)
        {
            if (possibleOperators.Contains(regex[0])) //if its an operator: assign the child nodes and add it to the nodes list
            {
                char @operator = regex[0];
                regex.RemoveAt(0);

                HandleOperator(@operator, regex, out State entry, out State exit);

                root.AddTransition(new Transition(entry, '\u03B5'));
                exit.AddTransition(new Transition(final, '\u03B5'));

                // return the empty char because we already handled the child operators
                return '\0';
            }
            else if (char.IsLetter(regex[0])) //if its a variable, return the new node. The child gets set in the operator check
            {
                char c = regex[0];
                regex.RemoveAt(0);
                return c;
            }
            else // if its a comma or parenthesis, just go to the next character
            {
                regex.RemoveAt(0);
                return AssignNodes(regex, root, final);
            }
        }

        private void HandleOperator(char @operator, List<char> remainingRegex, out State entry, out State exit)
        {
            switch (@operator)
            {
                case '|':
                    HandleOr(remainingRegex, out entry, out exit);
                    break;
                case '.':
                    HandleConcat(remainingRegex, out entry, out exit);
                    break;
                case '*':
                    HandleInfinite(remainingRegex, out entry, out exit);
                    break;
                // default shouldnt happen
                default:
                    entry = null;
                    exit = null;
                    break;
            }
        }

        // (|(left, right)): (q0, _ --> q1) (q0, _ --> q2) (q1,left --> q3) (q2,right --> q4) (q3,_ --> q5) (q4,_ --> q5)
        private void HandleOr(List<char> remainingRegex, out State entry, out State exit)
        {
            // add the 6 states we will need for this block and save them into the overall state list with unique incremental names. store the names locally for later use
            List<string> stateNames = new List<string>();

            // divide in two branches
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            states.Find(state => state.Name == stateNames[0]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[1]), '\u03B5'));
            
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            states.Find(state => state.Name == stateNames[0]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[2]), '\u03B5'));

            // left
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            char nextChar = AssignNodes(remainingRegex, states.Find(state => state.Name == stateNames[1]), states.Find(state => state.Name == stateNames[3]));
            // single transition with letter
            if (char.IsLetter(nextChar))
            {
                states.Find(state => state.Name == stateNames[1]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[3]), nextChar));
            }
            // operator handled in AssignNodes

            // right
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            nextChar = AssignNodes(remainingRegex, states.Find(state => state.Name == stateNames[2]), states.Find(state => state.Name == stateNames[4]));
            // single transition with letter
            if (char.IsLetter(nextChar))
            {
                states.Find(state => state.Name == stateNames[2]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[4]), nextChar));
            }
            // operator handled in AssignNodes

            // come together in the single last state
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            states.Find(state => state.Name == stateNames[3]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[5]), '\u03B5'));
            states.Find(state => state.Name == stateNames[4]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[5]), '\u03B5'));

            // assign the correct out parameters for the calling method to use
            entry = states.Find(state => state.Name == stateNames[0]);
            exit = states.Find(state => state.Name == stateNames[stateNames.Count - 1]);
        }

        // .(left,right): (q0,left --> q1) (q1,right --> q2)
        private void HandleConcat(List<char> remainingRegex, out State entry, out State exit) 
        {
            // create and store states and names to use later
            List<string> stateNames = new List<string>();

            // left
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            char nextChar = AssignNodes(remainingRegex, states.Find(state => state.Name == stateNames[0]), states.Find(state => state.Name == stateNames[1]));
            // single transition with letter
            if (char.IsLetter(nextChar))
            {
                states.Find(state => state.Name == stateNames[0]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[1]), nextChar));
            }
            // operator handled in AssignNodes

            // right
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            nextChar = AssignNodes(remainingRegex, states.Find(state => state.Name == stateNames[1]), states.Find(state => state.Name == stateNames[2]));
            // single transition with letter
            if (char.IsLetter(nextChar))
            {
                states.Find(state => state.Name == stateNames[1]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[2]), nextChar));
            }
            // operator handled in AssignNodes

            // assign the correct out parameters for the calling method to use
            entry = states.Find(state => state.Name == stateNames[0]);
            exit = states.Find(state => state.Name == stateNames[stateNames.Count - 1]);
        }

        // *(left): (q0,_ --> q1) (q1,left --> q2) (q2,_ --> q1) (q2,_ --> q3) (q0,_ --> q3)
        private void HandleInfinite(List<char> remainingRegex, out State entry, out State exit) 
        {
            List<string> stateNames = new List<string>();

            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            states.Find(state => state.Name == stateNames[0]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[1]), '\u03B5'));

            
            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            char nextChar = AssignNodes(remainingRegex, states.Find(state => state.Name == stateNames[1]), states.Find(state => state.Name == stateNames[2]));
            if (char.IsLetter(nextChar))
            {
                states.Find(state => state.Name == stateNames[1]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[2]), nextChar));
            }
            // operator handled in AssignNodes

            states.Find(state => state.Name == stateNames[2]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[1]), '\u03B5'));

            stateNames.Add($"q{states.Count}");
            states.Add(new State($"q{states.Count}"));
            states.Find(state => state.Name == stateNames[2]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[3]), '\u03B5'));

            states.Find(state => state.Name == stateNames[0]).AddTransition(new Transition(states.Find(state => state.Name == stateNames[3]), '\u03B5'));

            // assign the correct out parameters for the calling method to use
            entry = states.Find(state => state.Name == stateNames[0]);
            exit = states.Find(state => state.Name == stateNames[stateNames.Count - 1]);
        }

        private List<char> GetAlphabet(string regex)
        {
            List<char> alphabet = new List<char>();

            regex.ToList().ForEach(character =>
            {
                if (char.IsLetter(character))
                {
                    alphabet.Add(character);
                }
            });

            return alphabet;
        }
    }
}
