﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finite_State_Automata_Calculator
{
    public static class Helper
    {
        /// <summary>
        /// Gets the part of the input up until the string specified exclusively.
        /// </summary>
        /// <param name="input">The string to get the result from.</param>
        /// <param name="stopAt">If this string is found, stop and return everything in front of it.</param>
        /// <returns>Returns the string from position 0 up until the string specified to stop at exclusively.</returns>
        public static string GetUntil(string input, string stopAt)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                int charLocation = input.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    string toReturn = input.Substring(0, charLocation);
                    return toReturn;
                }
            }

            return string.Empty;
        }
    }
}
